import React from 'react';
import axios from 'axios';

import ReactTable from 'react-table';
import 'react-table/react-table.css';
import Chart from '../charts/chart';

import './table.css';

class Table extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: null,
            error: false,
            loading: false,
            total: 0,
            chartData: {
                labels: ["Liquidity", "Fixed Income", "equity", "2050", 'ffdf'],
                datasets: [
                  {
                    label: "Current",
                    backgroundColor: "red",
                    data: [18.7, 34.53, 13.72, 7.84, 6.35]
                  }, {
                    label: "Proposed",
                    backgroundColor: "#000",
                    data: [6.89, 29.8, 27.84, 13.35, 4.78]
                  }
                ]
              },
              options: {
                title: {
                  display: true,
                  text: 'Population growth (millions)'
                }
              }
        }
    }

    total = () => {
      const { data } = this.state;
      let total = 0;
      data && data.map(d => total += d.id);
      this.setState({
        total
      })
    }
    componentDidMount() {
        this.setState({
            loading: true
        })
        axios.get('https://api.github.com/users')
            .then(({ data }) => {
                console.log(data);
                this.setState({
                    loading: false,
                    data
                });
                this.total();
            })
            .catch(error => {
                this.setState({
                    error
                })
            });

    }

    
    Footer = ()=> <div>fdfddf</div>

    render() {
        const { data, loading, error } = this.state;
        let total = 0;
      
        console.log(this.state);
        return (
            <React.Fragment>
              <div>
                <h2>Investment Proposal</h2>
                <h5>Proposed Asset Allocation</h5>

                <div>
                    Table - Current Asset Allocation vs Proposed Allocation
                </div>
                                
                <ReactTable
                loading={loading}
                PreviousComponent={this.Footer}                
                data= {data ? data : []}
                columns={[
                  {
                    Header: "Name",
                    columns: [
                      {
                        Header: "First Name",
                        accessor: "id",
                        Footer: (
                          <span className="table-footer">
                            <strong>Total</strong>
                          </span>
                        )
                      },
                      {
                        Header: "Last Name",
                        id: "login",
                        accessor: d => d.login,                      
                      }
                    ]                    
                  },
                  {
                    Header: "Info",
                    columns: [
                      {
                        Header: "Age",
                        accessor: "node_id",
                        Footer: (
                          <span className="table-footer">
                            <strong>Total</strong>{"  "}
                            {                                                  
                             this.state.total
                            }
                          </span>
                        )
                      },
                      {
                        Header: "Status",
                        accessor: "type"
                      }
                    ]
                  },
                  {
                    Header: 'Stats',
                    columns: [
                      {
                        Header: "Visits",
                        id: 'siteAdmin',
                        accessor: d => d.site_admin
                      }
                    ]
                  }
                ]}
                defaultPageSize={10}
                className="-highlight"
                />
            </div>
            <div style={{width: '50%'}}>
              <Chart data={this.state.chartData} />
            </div>
            </React.Fragment>

        )
    }


}

export default Table;