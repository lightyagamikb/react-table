import React from 'react';
import axios from 'axios';

import ReactTable from 'react-table';
import 'react-table/react-table.css';
import Chart from '../charts';

import './table.css';

class Table extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            totalData: null,
            error: false,
            loading: false,
            total: 0,
            chartData: {
                labels: ["Liquidity", "Fixed Income", "equity", "2050", 'ffdf'],
                datasets: [
                  {
                    label: "Current",
                    backgroundColor: "red",
                    data: [18.7, 34.53, 13.72, 7.84, 6.35]
                  }, {
                    label: "Proposed",
                    backgroundColor: "#000",
                    data: [6.89, 29.8, 27.84, 13.35, 4.78]
                  }
                ]
              },
              options: {
                title: {
                  display: true,
                  text: 'Population growth (millions)'
                }
              }
        }
    }

    total = () => {
      const { data } = this.state;
      let total = 0;
      data && data.map(d => total += d.id);
      this.setState({
        total
      })
      
    }

    componentDidMount() {
        this.setState({
            loading: true
        })
        axios.get('http://localhost:8080/sysapp/getAllAssetAllociations')
            .then(({ data }) => {
                console.log(data);
                this.setState({
                    loading: false,
                    totalData: data,
                });
                this.total();
            })
            .catch(error => {
                this.setState({
                    error
                })
            });

    }
 
    

    render() {
        const { totalData, loading, error } = this.state;
        let total = 0;
      
        console.log(this.state);
        return (
            <React.Fragment>
              <div className="tblWrapper">
                <h2>Investment Proposal</h2>
                <h5>Proposed Asset Allocation</h5>
                <div className="tbl-title">
                    Table - Current Asset Allocation vs Proposed Asset Allocation
                </div>
                                
                <ReactTable
                loading={false}                
                showPagination={false}             
                data= {totalData && totalData.assetClassification ? totalData.assetClassification : []}
                columns={[
                  {            
                    Header:  totalData && totalData.mainAccountNumber,
                    headerClassName:'tbl-header acc-no',
                    columns: [
                      {
                        Header: "Asset Clasification",
                        accessor: "displayName",
                        Footer: (
                          <span className="table-footer">
                            Total
                          </span>
                        )
                      }
                    ]                    
                  },
                  {
                    Header: "Current Asset Allocation",
                    headerClassName:'tbl-header currentAsset-header',
                    
                    columns: [
                      {
                        Header: "Amount",
                        id:"currentPortfolioAmount",
                        accessor:d => d.current.portfolioAmount,
                        Footer: (
                          <div className="table-footer text-right">                           
                            {                                                  
                             this.state.total
                            }
                          </div>
                        )
                      },
                      {
                        Header: "% Total",
                        id:"currentPortfolioWeightPercent",
                        accessor: d => d.current.portfolioWeightPercent || "-"
                      }
                    ]
                  },
                  {
                    Header: 'Net',
                    headerClassName:'tbl-header',
                    columns: [
                      {
                        Header: "Change",
                        id: 'siteAdmin',
                        accessor: d => d.site_admin || "-"
                      }
                    ]
                  },
                  {
                    Header: 'Proposed Asset Allocation',
                    headerClassName:'tbl-header proposedAsset-header',
                    columns: [
                      {
                        Header: "Amount",
                        id: "proposedPortfolioAmount",
                        accessor: d => d.proposed.portfolioAmount
                      },
                      {
                        Header: "% Total",
                        id: "proposedPortfolioWeightPercet",
                        accessor: d => d.proposed.portfolioWeightPercent
                      }
                    ]
                  }
                ]}
                defaultPageSize={6}
                className="-highlight"
                />
            </div>
              <div className="chart-wrapper"> 
                  <Chart data={this.state.chartData} className="asset-allocation-chart"/>
                  <div className="asset-allocation-commentary">
                    <h4>Asset Allocation Commentary</h4>
                    <p> [Please enter key high-level information about the Investment Proposal</p>
                  </div>
                </div>            
            </React.Fragment>

        )
    }


}

export default Table;