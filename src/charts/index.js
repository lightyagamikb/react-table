import React from 'react';
import { Bar, Pie } from 'react-chartjs-2';


class Chart extends React.Component {

    static defaultProps = {
        displayTitle:false,
        displayLegend: true,
        legendPosition:'right',
        text: ''
    }

    render() {
        return(                      
           <div className="chart-info">
                 <h4 className="chart-title">
                    Chart - Current Asset Allocation vs Proposed Asset Allocation
                </h4> 
                <Bar                
                data={this.props.data}
                options={{
                    title:{
                        display:this.props.displayTitle,
                        text:this.props.text,
                        fontSize:25
                    },
                    legend:{
                        display:this.props.displayLegend,
                        position:this.props.legendPosition
                    },
                    // maintainAspectRatio: false
                }}
            />
           </div>                         
        )
    }
}

export default Chart;